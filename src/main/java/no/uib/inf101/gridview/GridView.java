package no.uib.inf101.gridview;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {

   public static final double OUTERMARGIN = 30;
   public static final Color MARGINCOLOR = Color.LIGHT_GRAY;

   public IColorGrid colorGrid; // The grid with colors for the cells
   public Rectangle2D outerBox; // The bounding box for the grid area

   public GridView(IColorGrid colorGrid) {
      this.setPreferredSize(new Dimension(400, 300));
      this.colorGrid = colorGrid; // Assign the passed IColorGrid to the class member
   }

   @Override
   public void paintComponent(Graphics g) {
       super.paintComponent(g);
       Graphics2D g2 = (Graphics2D) g;
       this.outerBox = calculateOuterBox();
       drawGrid(g2); // Call drawGrid with the Graphics2D object
   }

   public Rectangle2D calculateOuterBox() {
      double x = OUTERMARGIN;
      double y = OUTERMARGIN;
      double width = getWidth() - (OUTERMARGIN * 2);
      double height = getHeight() - (OUTERMARGIN * 2);
      return new Rectangle2D.Double(x, y, width, height);
   }

   public void drawGrid(Graphics2D graphics2d) {
      graphics2d.setColor(MARGINCOLOR);
      graphics2d.fill(outerBox); // Fill the outer box with the margin color
      CellPositionToPixelConverter converter = new CellPositionToPixelConverter(outerBox, colorGrid, OUTERMARGIN);
      drawCells(graphics2d, colorGrid, converter); // Call drawCells with the converter
   }

   public static void drawCells(Graphics2D graphics2d, CellColorCollection colorCollection, CellPositionToPixelConverter converter) {
       for (CellColor cell : colorCollection.getCells()) {
           Color color = (cell.color() != null) ? cell.color() : Color.DARK_GRAY;
           graphics2d.setColor(color);
           Rectangle2D cellBounds = converter.getBoundsForCell(cell.cellPosition());
           graphics2d.fill(cellBounds); // Draw the cell
       }
   }
}


  // private static void drawCells(Graphics2D g2d, CellColorCollection cells, CellPositionToPixelConverter converter){
  //  for (CellColor cell : cells.getCells()){
  //    g2d.setColor(cell.color() != null ? cell.color() : Color.DARK_GRAY);
  //  }
 //  }








/*
    public GridView(IColorGrid colorgrid) {
        this.setPreferredSize(new Dimension(400, 300));
        this.colorgrid = colorgrid;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        this.drawGrid(g2);
    }

    private void drawGrid(Graphics2D graphics2D) {
        double margin = 30;
        double x = margin;
        double y = margin;
        double width = this.getWidth() - 2 * margin;
        double height = this.getHeight() - 2 * margin; // Corrected to getHeight()
        Rectangle2D.Double box = new Rectangle2D.Double(x, y, width, height);

        graphics2D.setColor(Color.lightGray);
        graphics2D.fill(box);

        CellPositionToPixelConverter cellConverter = new CellPositionToPixelConverter(box, colorgrid, margin);
        drawCells(graphics2D, colorgrid, cellConverter); // This method is now package-private
    }

    // Package-private to allow testing, could also be protected if needed
    void drawCells(Graphics2D graphics2d, IColorGrid colorgrid2, CellPositionToPixelConverter cellConverter) {
        for (CellColor cell : colorgrid.getCells()) {
            if (cell.color() != null) {
                graphics2d.setColor(cell.color());
            } else {
                graphics2d.setColor(Color.darkGray);
            }
            graphics2d.fill(cellConverter.getBoundsForCell(cell.cellPosition()));
        }
    }
}

  
*/
