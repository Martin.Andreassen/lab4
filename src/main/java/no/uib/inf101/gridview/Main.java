package no.uib.inf101.gridview;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.ColorGrid;
//import no.uib.inf101.gridview.GridView.Colorgrid;

public class Main {
  public static void main(String[] args) {
    // TODO: Implement this method


    ColorGrid colorGrid = new ColorGrid(10, 10);

    GridView gridView = new GridView(colorGrid);
    JFrame frame = new JFrame();
    frame.setTitle("INF101");
    frame.setContentPane(gridView);

    
    frame.pack();
    frame.setDefaultCloseOperation(0);
    frame.setTitle(null);
    frame.setVisible(true);


    
  }
}
