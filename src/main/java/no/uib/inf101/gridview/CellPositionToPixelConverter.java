package no.uib.inf101.gridview;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
    
  
  
    public Rectangle2D box; 
    public GridDimension gd;   
    public double margin;
    
    
    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
      this.box = box;
      this.gd = gd;
      this.margin = margin;
  }
  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double cellWidth = (box.getWidth() - ((gd.cols() + 1) * margin)) / gd.cols();
    double cellHeight = (box.getHeight() - ((gd.rows() + 1) * margin)) / gd.rows();
    double x = box.getX() + margin + (cp.col() * (cellWidth + margin));
    double y = box.getY() + margin + (cp.row() * (cellHeight + margin));
    return new Rectangle2D.Double(x, y, cellWidth, cellHeight);
}
  
}



 /*   
    public CellPositionToPixelConverter(Rectangle2D.Double box, IColorGrid colorgrid, double margin) {
        this.box = box;
        this.colorgrid = colorgrid;
        this.margin = margin;
    }

    public Shape getBoundsForCell(CellPosition cellPosition) {
        int totalCols = colorgrid.cols();
        int totalRows = colorgrid.rows();
        double cellWidth = (box.width - (margin * (totalCols + 1))) / totalCols;
        double cellHeight = (box.height - (margin * (totalRows + 1))) / totalRows;
        double x = box.x + margin + (cellPosition.col() * (cellWidth + margin));
        double y = box.y + margin + (cellPosition.row() * (cellHeight + margin));
        return new Rectangle2D.Double(x, y, cellWidth, cellHeight);
    }
*/
  
