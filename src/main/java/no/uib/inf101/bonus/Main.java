package no.uib.inf101.bonus;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.gridview.GridView;

public class Main {
  public static void main(String[] args) {
    IColorGrid grid = new ColorGrid(3, 4);
    
    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    
    GridView gridView = new GridView(grid);
    JFrame jFrame = new JFrame();
    
    jFrame.setContentPane(gridView);
    jFrame.setTitle("Firkant");
    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jFrame.pack();
    jFrame.setVisible(true);
  }
  
}



