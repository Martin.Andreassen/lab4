package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
    private int numRows;
    private int numCols;
    private Color[][] grid;

    public ColorGrid(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.grid = new Color[numRows][numCols];
    }

    @Override
    public int rows() {
        return numRows;
    }

    @Override
    public int cols() {
        return numCols;
    }

    @Override
    public List<CellColor> getCells() {
        List<CellColor> list = new ArrayList<>();
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                CellPosition position = new CellPosition(row, col);
                Color color = grid[row][col];
                list.add(new CellColor(position, color));
            }
        }
        return list;
    }

    @Override
    public Color get(CellPosition pos) {
        return grid[pos.row()][pos.col()];
    }

    public void set(CellPosition pos, Color color) {
        grid[pos.row()][pos.col()] = color;
    }
}
